# A quickstart Project to run Web App

---


###### INSTALL

* Clone Repository git clone https://fede_iglesiasc@bitbucket.org/fede_iglesiasc/quickstart-webapp.git
* npm run install


---


###### RUN & FUN

###### For developement

* npm run dev
* Enter with browser to http://localhost:3000

###### For production

* npm run build
* npm run start
* Enter with browser to http://localhost:3070 (you can change this in package.json)

---

###### Libs used

* **NextJS:** minimalistic framework for server-rendered React Apps.
* **React:** library for building user interfaces.
* **Redux:** predictable state container for JavaScript Apps.
* **Material-ui:** React components that implement Google's Material Design.
* **Lodash:** utility library delivering modularity, performance, & extras.